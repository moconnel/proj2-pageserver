Author: Michael O'Connell, moconnel@uoregon.edu

A "getting started" project for using Docker and Flask. Project is for CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

