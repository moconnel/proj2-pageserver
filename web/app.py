from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route("/<path:name>")
def index(name):
    if "~" in name or "//" in name or ".." in name:
        return page_forbidden(403)
    elif not render_template(name):
        return page_not_found(404)
    else:
        return render_template(name)

@app.errorhandler(403)
def page_forbidden(e):
    return render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
